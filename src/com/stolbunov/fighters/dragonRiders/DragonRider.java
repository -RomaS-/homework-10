package com.stolbunov.fighters.dragonRiders;

import com.stolbunov.dangeon.IMazeRunner;
import com.stolbunov.fighters.ArenaFighters;
import com.stolbunov.fighters.dragons.Dragon;

public class DragonRider extends ArenaFighters {
    private Dragon ridingDragon;
    private float healthDragon;

    public DragonRider(String name, float health, float damage, float armor) {
        super(name, health, damage, armor);
    }

    @Override
    public float attack(ArenaFighters var1) {
        if (var1 instanceof Dragon) {
            attackDragon((Dragon) var1);
            return 0;
        } else {
            return var1.damaged(this.damage);
        }
    }

    private void attackDragon(Dragon dragon) {
        this.ridingDragon = dragon;
        healthDragon = dragon.getHealth();
        dragon.setHealth(0);
    }
}
