package com.stolbunov.fighters;

public interface PostAttackAction {
    void postAttackAction(float damageTaken, float damageGotten);
}
