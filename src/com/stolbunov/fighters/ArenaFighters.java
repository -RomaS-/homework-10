package com.stolbunov.fighters;

import com.stolbunov.dangeon.IMazeRunner;

public abstract class ArenaFighters implements IMazeRunner {
    protected String name;
    protected float health;
    protected float damage;
    protected float armor;
    protected float maxHealth;
    private int coordinateX = 0;
    private int coordinateY = 0;

    public ArenaFighters(String name, float health, float damage, float armor) {
        this.name = name;
        this.health = health;
        this.damage = damage;
        this.armor = armor;
        this.maxHealth = health;
    }

    public boolean isAlfie() {
        return health > 0;
    }

    public String getName() {
        return this.name;
    }

    public void heal(float health) {
        float newHeal = this.health + health;
        this.health = Math.min(newHeal, this.maxHealth);
    }

    public abstract float attack(ArenaFighters var1);

    public float damaged(float damageTaken) {
        float resist = (damageTaken * armor);
        float gotDamage = damageTaken - resist;
        health -= gotDamage;
        System.out.println(name + " got damage " + gotDamage + " resisted "
                + resist + " health left " + health);
        return gotDamage;
    }

    public float getMaxHealth() {
        return maxHealth;
    }

    public float getHealth() {
        return health;
    }

    public void setHealth(float health) {
        this.health = health;
    }

    @Override
    public int[][] goForward() {
        coordinateX++;
        return new int[][]{{coordinateY, coordinateX}};
    }

    @Override
    public int[][] goBackward() {
        coordinateX--;
        return new int[][]{{coordinateY, coordinateX}};
    }

    @Override
    public int[][] goRight() {
        coordinateY++;
        return new int[][]{{coordinateY, coordinateX}};
    }

    @Override
    public int[][] goLeft() {
        coordinateY--;
        return new int[][]{{coordinateY, coordinateX}};
    }

    @Override
    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    @Override
    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    @Override
    public void setHP(float health) {
        setHealth(health);
    }

    @Override
    public float getHP() {
        return getHealth();
    }

    @Override
    public float getMaxHP() {
        return getMaxHealth();
    }
}
