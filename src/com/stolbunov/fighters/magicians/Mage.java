package com.stolbunov.fighters.magicians;

import com.stolbunov.fighters.ArenaFighters;
import com.stolbunov.fighters.Elements;

public class Mage extends ArenaFighters implements Elements {
    int element;

    public Mage(String name, float health, float damage, float armor, int element) {
        super(name, health, damage, armor);
        this.element = element;
    }

    @Override
    public float attack(ArenaFighters fighters) {
        if (fighters instanceof Elements) {
            if (isElementsEquals(((Elements) fighters).getElements())) {
                return 0;
            }
        }
        return fighters.damaged(damage);
    }

    @Override
    public int getElements() {
        return element;
    }
}
