package com.stolbunov.fighters.knights;

import com.stolbunov.fighters.ArenaFighters;

import java.util.Random;

public class Knight extends ArenaFighters {
    protected float shield;

    public Knight(String name, float health, float damage, float armor, float shield) {
        super(name, health, damage, armor);
        this.shield = shield;
    }

    public float attack(ArenaFighters arenaFighter) {
        return arenaFighter.damaged(this.damage);
    }

    public float damaged(float damageTaken) {
        Random random = new Random();
        if (random.nextGaussian() > (double) this.shield) {
            return super.damaged(damageTaken);
        } else {
            System.out.println(this.name + " blocked");
        }
        return 0;
    }
}
