package com.stolbunov.dangeon;

import com.stolbunov.ILabyrinth;
import com.stolbunov.arenas.LabyrinthDuelArena;
import com.stolbunov.elixirs.ElixirPoison;
import com.stolbunov.elixirs.ElixirRecovery;
import com.stolbunov.fighters.ArenaFighters;

import java.util.Arrays;

public abstract class BaseLabyrinth implements ILabyrinth {
    protected char iconEmptySquare = '\u2610';
    protected char iconLabeledSquare = '\u2612';
    protected char iconFinish = '\u2FA8';
    protected char iconBarrier = '\u2617';
    protected char iconSmilingSmiley = '\u263B';
    protected char iconWhiteFlag = '\u2690';
    protected char iconBlackFlag = '\u2691';
    protected char iconElixirRecovery = '\u2764';
    protected char iconElixirPoison = '\u2622';
    protected char iconEnemy = '\u265e';
    protected int participantCoordinateY = 0;
    protected int participantCoordinateX = 0;
    protected int finishCoordinateY;
    protected int finishCoordinateX;
    protected int counterToWin;
    protected int amountEnemy;
    protected int numEnemy = 0;
    protected char[][] labyrinth;
    protected ArenaFighters[] enemies;
    protected IMazeRunner participant;

    protected int amountBarrier;
    protected int amountElixirRecovery;
    protected int amountElixirPoison;

    public BaseLabyrinth(int height, int width, int amountBarrier, int amountElixirRecovery,
                         int amountElixirPoison, IMazeRunner participant, ArenaFighters... enemies) {
        labyrinth = new char[height][width];
        finishCoordinateY = height - 1;
        finishCoordinateX = width - 1;
        this.participant = participant;
        counterToWin = height * width - amountBarrier;
        this.enemies = enemies;
        amountEnemy = enemies.length;
        this.amountBarrier = amountBarrier;
        this.amountElixirRecovery = amountElixirRecovery;
        this.amountElixirPoison = amountElixirPoison;
        decorationLabyrinth();
    }

    protected abstract void movementParticipant();

    protected void decorationLabyrinth() {
        for (int i = 0; i < labyrinth.length; i++) {
            for (int j = 0; j < labyrinth[i].length; j++) {
                labyrinth[i][j] = iconEmptySquare;
            }
        }
        creatingObstacles();
        creatingFinish();
    }

    protected void creatingObstacles() {
        creatingNumberOfDecorElements(amountBarrier, iconBarrier);
        creatingNumberOfDecorElements(amountElixirRecovery, iconElixirRecovery);
        creatingNumberOfDecorElements(amountElixirPoison, iconElixirPoison);
        creatingNumberOfDecorElements(amountEnemy, iconEnemy);
    }

    protected void creatingNumberOfDecorElements(int num, char icon) {
        for (int i = 0; i < num; i++) {
            creatingOneElementOfDecor(icon);
        }
    }

    protected void creatingOneElementOfDecor(char icon) {
        int y = (int) (Math.random() * (finishCoordinateY - 1) + 1);
        int x = (int) (Math.random() * (finishCoordinateX - 1) + 1);
        if (isIconInCell(y, x, iconEmptySquare)) {
            labyrinth[y][x] = icon;
        } else {
            creatingOneElementOfDecor(icon);
        }
    }

    protected void creatingFinish() {
        if (dropTheCoin()) {
            labyrinth[finishCoordinateY][finishCoordinateX] = iconFinish;
        }
    }

    protected boolean isGameOver() {
        if (labyrinth[participantCoordinateY][participantCoordinateX] == iconFinish) {
            markWay();
            seeMap();
            System.out.println("The participant successfully comes to the Finish!" + iconWhiteFlag + iconBlackFlag
                    + iconWhiteFlag + " Not visited cells " + counterToWin);
            return true;
        } else if ((counterToWin == 0)) {
            markWay();
            seeMap();
            System.out.println("The labyrinth passed completely but it has no way out!");
            return true;
        } else if (participant.getHP() <= 0) {
            System.out.println("Your hero is dead " + '\u2639');
            return true;
        }
        return false;
    }

    protected void markWay() {
        if (isReplaceIcon()) {
            labyrinth[participantCoordinateY][participantCoordinateX] = iconLabeledSquare;
            counterToWin();
        } else if (labyrinth[participantCoordinateY][participantCoordinateX] == iconFinish) {
            counterToWin();
            labyrinth[participantCoordinateY][participantCoordinateX] = iconSmilingSmiley;
        }
    }

    protected boolean isReplaceIcon() {
        return isIconInCell(participantCoordinateY, participantCoordinateX, iconEmptySquare)
                || isIconInCell(participantCoordinateY, participantCoordinateX, iconElixirRecovery)
                || isIconInCell(participantCoordinateY, participantCoordinateX, iconElixirPoison)
                || isIconInCell(participantCoordinateY, participantCoordinateX, iconEnemy);
    }

    protected void seeMap() {
        for (char[] lab : labyrinth) {
            System.out.println(Arrays.toString(lab));
        }
    }

    protected void counterToWin() {
        counterToWin--;
    }


    protected boolean isSuccessfulMovement(int[][] coordinatesMovement) {
        if (coordinatesMovement == null || coordinatesMovement[0].length < 2) {
            return false;
        } else if ((coordinatesMovement[0][0] < labyrinth.length && coordinatesMovement[0][0] >= 0)
                && (coordinatesMovement[0][1] < labyrinth[0].length && coordinatesMovement[0][1] >= 0)) {
            return !(isIconInCell(coordinatesMovement[0][0], coordinatesMovement[0][1], iconBarrier));
        } else {
            return false;
        }
    }

    protected boolean isSuccessfulMovement(int coordinateY, int coordinateX) {
        int[][] coordinates = new int[][]{{coordinateY, coordinateX}};
        return isSuccessfulMovement(coordinates);
    }

    protected boolean isIconInCell(int coordinateY, int coordinateX, char icon) {
        return labyrinth[coordinateY][coordinateX] == icon;
    }

    protected void eventsOnWay() {
        if (isIconInCell(participantCoordinateY, participantCoordinateX, iconElixirRecovery)) {
            openElixir(new ElixirRecovery());
        } else if (isIconInCell(participantCoordinateY, participantCoordinateX, iconElixirPoison)) {
            openElixir(new ElixirPoison());
        } else if (isIconInCell(participantCoordinateY, participantCoordinateX, iconEnemy)) {
            startBattle(new LabyrinthDuelArena());
        }
    }

    protected void startBattle(FindEnemy enemy) {
        enemy.battle(participant, enemies[numEnemy++]);
    }

    protected void openElixir(FindElixir findElixir) {
        findElixir.open(participant);
    }

    protected boolean dropTheCoin() {
        int randomNum = (int) (Math.random() * 100);
        return (randomNum % 2) == 0;
    }
}
