package com.stolbunov.dangeon;

import com.stolbunov.fighters.ArenaFighters;

public interface FindEnemy {
    void battle(IMazeRunner IMazeRunner, ArenaFighters participant);
}
