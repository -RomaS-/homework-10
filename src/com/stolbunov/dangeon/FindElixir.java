package com.stolbunov.dangeon;

public interface FindElixir {
    void open(IMazeRunner participant);
}
