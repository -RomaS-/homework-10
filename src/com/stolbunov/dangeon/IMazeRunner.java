package com.stolbunov.dangeon;

public interface IMazeRunner {
    int[][] goForward();

    int[][] goBackward();

    int[][] goRight();

    int[][] goLeft();

    void setCoordinateY(int coordinateY);

    void setCoordinateX(int coordinateX);

    void setHP(float health);

    float getHP();

    float getMaxHP();


}
