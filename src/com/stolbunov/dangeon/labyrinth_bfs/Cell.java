package com.stolbunov.dangeon.labyrinth_bfs;

import java.util.Objects;

public class Cell {
    private int y;
    private int x;

    public Cell(int y, int x) {
        this.y = y;
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cell)) return false;
        Cell cell = (Cell) o;
        return getY() == cell.getY() &&
                getX() == cell.getX();
    }

    @Override
    public String toString() {
        return "Cell[" + y + ":" + x +"]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(getY(), getX());
    }
}
