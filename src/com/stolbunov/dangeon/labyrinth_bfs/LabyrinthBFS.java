package com.stolbunov.dangeon.labyrinth_bfs;

import com.stolbunov.dangeon.BaseLabyrinth;
import com.stolbunov.dangeon.IMazeRunner;
import com.stolbunov.fighters.ArenaFighters;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class LabyrinthBFS extends BaseLabyrinth {
    private static final int MIN_HEIGHT = 20;
    private static final int MIN_WIDTH = 20;

    private static int amountBarrier = 10;
    private static int amountElixirRecovery = 3;
    private static int amountElixirPoison = 3;
    private Queue<Cell> listAvailableCell;
    private Set<Cell> listTraversedCells;
    private int[][] alternativeCoordinates = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};

    public LabyrinthBFS(IMazeRunner participant, ArenaFighters... enemies) {
        super(MIN_HEIGHT, MIN_WIDTH, amountBarrier, amountElixirRecovery, amountElixirPoison, participant, enemies);
        listAvailableCell = new LinkedList<>();
        listAvailableCell.add(new Cell(participantCoordinateY, participantCoordinateX));
        listTraversedCells = new LinkedHashSet<>();
    }

    @Override
    public void startWay() {
        while (!isGameOver()) {
            seeMap();
            movementParticipant();
            markWay();
        }
        System.out.println("Entire route of the participant: " + listTraversedCells.toString());
    }

    @Override
    protected void movementParticipant() {
        if (listAvailableCell.size() > 0) {
            Cell currentCell = listAvailableCell.poll();
            participantCoordinateY = currentCell.getY();
            participantCoordinateX = currentCell.getX();
            eventsOnWay();
            searchAlternativeWays(participantCoordinateY, participantCoordinateX);
            listTraversedCells.add(currentCell);
            announcement(participantCoordinateY, participantCoordinateX);
        }
    }


    private void searchAlternativeWays(int participantCoordinateY, int participantCoordinateX) {
        for (int[] alternativeCoordinate : alternativeCoordinates) {
            int newCoordinatesY = participantCoordinateY + alternativeCoordinate[0];
            int newCoordinatesX = participantCoordinateX + alternativeCoordinate[1];
            if (isSuccessfulMovement(newCoordinatesY, newCoordinatesX)) {
                Cell cell = new Cell(newCoordinatesY, newCoordinatesX);
                if (!listAvailableCell.contains(cell) && !listTraversedCells.contains(cell))
                    listAvailableCell.add(cell);
            }
        }
    }

    private void announcement(int participantCoordinateY, int participantCoordinateX) {
        System.out.println("The participant is at the mark [" + participantCoordinateY + ", " + participantCoordinateX +
                "] The mood is excellent. It remains to visit " + counterToWin + " cells. Number of remaining HP "
                + (int) participant.getHP());
    }
}