package com.stolbunov.dangeon.labytinth_standatd;

import com.stolbunov.dangeon.BaseLabyrinth;
import com.stolbunov.dangeon.IMazeRunner;
import com.stolbunov.fighters.ArenaFighters;

public class LabyrinthStandard extends BaseLabyrinth {
    private static final int MIN_HEIGHT = 10;
    private static final int MIN_WIDTH = 10;

    private static int amountBarrier = 7;
    private static int amountElixirRecovery = 3;
    private static int amountElixirPoison = 3;
    private String courseParticipant;

    public LabyrinthStandard(IMazeRunner participant, ArenaFighters... enemies) {
        super(MIN_HEIGHT, MIN_WIDTH, amountBarrier, amountElixirRecovery, amountElixirPoison, participant, enemies);
    }

    @Override
    public void startWay() {
        while (!isGameOver()) {
            markWay();
            seeMap();
            movementParticipant();
        }
    }

    @Override
    protected void movementParticipant() {
        int[][] coordinates = generationRandomMove();
        String access;
        if (isSuccessfulMovement(coordinates)) {
            readCoordinatesParticipant(coordinates);
            eventsOnWay();
            access = " Path is free.";
        } else {
            returnParticipantBack();
            access = " On a way Deadlock remain on [" + participantCoordinateY + "]" +
                    "[" + participantCoordinateX + "].";
        }
        announcement(courseParticipant, access, coordinates);
    }

    private int[][] generationRandomMove() {
        int[][] coordinatesMovement = null;
        int randomMove = (int) ((Math.random() * 4));
        switch (randomMove) {
            case 0:
                courseParticipant = "Forward";
                coordinatesMovement = participant.goForward();
                break;
            case 1:
                courseParticipant = "Backward";
                coordinatesMovement = participant.goBackward();
                break;
            case 2:
                courseParticipant = "Right";
                coordinatesMovement = participant.goRight();
                break;
            case 3:
                courseParticipant = "Left";
                coordinatesMovement = participant.goLeft();
                break;
        }
        return coordinatesMovement;
    }

    private void readCoordinatesParticipant(int[][] coordinatesMovement) {
        participantCoordinateY = coordinatesMovement[0][0];
        participantCoordinateX = coordinatesMovement[0][1];
    }

    private void returnParticipantBack() {
        participant.setCoordinateY(participantCoordinateY);
        participant.setCoordinateX(participantCoordinateX);
    }

    private void announcement(String direction, String access, int[][] coordinate) {
        int coordinateY = participantCoordinateY;
        int coordinateX = participantCoordinateX;
        if (coordinate != null && coordinate[0].length > 1) {
            coordinateY = coordinate[0][0];
            coordinateX = coordinate[0][1];
        }
        System.out.println("The participant tries to move to the " + direction + " to the [" + coordinateY + "]" +
                "[" + coordinateX + "] mark. " + access + " Not visited cells " + counterToWin +
                ". The amount of the participant's remaining life is " + (int) participant.getHP());
    }
}
