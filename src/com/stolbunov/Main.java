package com.stolbunov;

import com.stolbunov.dangeon.labyrinth_bfs.LabyrinthBFS;
import com.stolbunov.dangeon.IMazeRunner;
import com.stolbunov.dangeon.labytinth_standatd.LabyrinthStandard;
import com.stolbunov.fighters.ArenaFighters;
import com.stolbunov.fighters.dragonRiders.DragonRider;
import com.stolbunov.fighters.dragons.Dragon;
import com.stolbunov.fighters.knights.BlackKinght;
import com.stolbunov.fighters.knights.HollyKnight;
import com.stolbunov.healers.Healer;

public class Main {
    public static void main(String[] args) {
        Healer healer = new Healer("Jorge", 5);

        IMazeRunner stepan = new Dragon("Stepan", 8000, 15, 0.5f, Dragon.WATER | Dragon.WIND);
        Dragon chuck = new Dragon("Chuck", 150, 14, 0.3f, Dragon.EARTH | Dragon.FIRE);
        Dragon adolfo = new Dragon("Adolfo", 100, 10, 0.4f, Dragon.EARTH | Dragon.WIND);
        Dragon freddy = new Dragon("", 4000, 10, 0.4f, Dragon.EARTH | Dragon.WIND);
        DragonRider bobby = new DragonRider("Bobby", 500, 10, 5);
        ArenaFighters hollyKnight = new HollyKnight("HollyKnight", 100, 15, 0.15f, 2, 5);
        ArenaFighters blackKnight = new BlackKinght("BlackKnight", 150, 40, 0.5f, 1);

        ILabyrinth labyrinthBFS = new LabyrinthBFS(stepan, adolfo, chuck);
        labyrinthBFS.startWay();

//        ILabyrinth labyrinthStandard = new LabyrinthStandard(stepan, adolfo, chuck);
//        labyrinthStandard.startWay();
    }
}
