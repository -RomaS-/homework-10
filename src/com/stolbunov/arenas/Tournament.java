package com.stolbunov.arenas;

import com.stolbunov.fighters.*;
import com.stolbunov.healers.Healer;


public class Tournament extends BattleArena {
    private int roundCountMax;
    private ArenaFighters[] tournamentWinner;
    private ArenaFighters[] participants;

    public Tournament(Healer healer, ArenaFighters participant1, ArenaFighters participant2, ArenaFighters participant3,
                      ArenaFighters participant4, ArenaFighters participant5, ArenaFighters participant6,
                      ArenaFighters participant7, ArenaFighters participant8, int roundCountMax) {
        super(healer);
        this.roundCountMax = roundCountMax;
        participants = new ArenaFighters[]{participant1, participant2, participant3, participant4, participant5,
                participant6, participant7, participant8};
    }

    public int getNumberOfParticipants() {
        return participants.length;
    }

    @Override
    public void startBattle() {
        tournamentWinner = battleInTournament(participants);
    }

    @Override
    public void printWinner() {
        if (tournamentWinner != null) {
            System.out.println("The winner of the tournament of " + getNumberOfParticipants() +
                    " participants becomes " + tournamentWinner[0].getName());
        } else {

        }
    }

    private ArenaFighters[] battleInTournament(ArenaFighters[] participants){
        int index = 0;
        int counter = 0;
        if (participants.length > 1) {
            ArenaFighters[] winners = new ArenaFighters[participants.length / 2];
            for (int i = 1; i < participants.length; i++) {
                if (i % 2 == 0) {
                    continue;
                }

                while (counter < roundCountMax) {
                    confrontation(participants[i-1], participants[i]);
                    counter++;
                }

                ArenaFighters win = calculationOfWinner(participants[i-1], participants[i]);
                winners[index++] = win;
                System.out.println("<<<" + participants[i-1].getName() + " VS " + participants[i].getName() + ">>>" +
                        " WINS " + win.getName());
            }
            participants = battleInTournament(winners);
        }
        return participants;
    }
}
