package com.stolbunov.arenas;

import com.stolbunov.dangeon.FindEnemy;
import com.stolbunov.dangeon.IMazeRunner;
import com.stolbunov.fighters.ArenaFighters;

public class LabyrinthDuelArena extends DuelArena implements FindEnemy {
    public LabyrinthDuelArena() {
        super(null, null, null, 0);
    }

    @Override
    public void startBattle() {
        setStartRound(true);
        while (isFightContinue(participant1, participant2)) {
            confrontation(participant1, participant2);
        }
    }

    @Override
    public void battle(IMazeRunner IMazeRunner, ArenaFighters participant) {
        if (IMazeRunner instanceof ArenaFighters) {
            participant1 = (ArenaFighters) IMazeRunner;
            participant2 = participant;
            startBattle();
            printWinner();
        }
    }
}
