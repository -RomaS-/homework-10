package com.stolbunov.arenas;

import com.stolbunov.fighters.ArenaFighters;
import com.stolbunov.healers.Healer;

public class DuelArena extends BattleArena {
    protected ArenaFighters participant1;
    protected ArenaFighters participant2;
    private int roundCountMax;


    public DuelArena(Healer healer, ArenaFighters participant1, ArenaFighters participant2, int roundCountMax) {
        super(healer);
        this.participant1 = participant1;
        this.participant2 = participant2;
        this.roundCountMax = roundCountMax;
    }

    @Override
    public void startBattle() {
        setStartRound(true);
        int currentRound = 0;
        while (currentRound < roundCountMax && isFightContinue(participant1, participant2)) {
            confrontation(participant1, participant2);
            currentRound++;
        }
    }

    @Override
    public void printWinner() {
        if (isStartRound()) {
            ArenaFighters winners = calculationOfWinner(participant1, participant2);
            if (winners != null) {
                System.out.println("In the battle wins " + winners.getName() + "!");
            } else {
                System.out.println("Not all participants gathered at the start!");
            }

        } else {
            System.out.println("The battle has not yet begun!");
        }
    }
}
