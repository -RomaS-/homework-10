package com.stolbunov.elixirs;

import com.stolbunov.dangeon.FindElixir;
import com.stolbunov.dangeon.IMazeRunner;

public class ElixirPoison implements FindElixir {
    @Override
    public void open(IMazeRunner participant) {
        float damage = 0.3f * participant.getHP();
        participant.setHP(damage);
    }
}
