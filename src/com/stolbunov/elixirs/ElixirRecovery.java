package com.stolbunov.elixirs;

import com.stolbunov.dangeon.FindElixir;
import com.stolbunov.dangeon.IMazeRunner;

public class ElixirRecovery implements FindElixir {
    @Override
    public void open(IMazeRunner participant) {
        participant.setHP(participant.getMaxHP());
    }
}
